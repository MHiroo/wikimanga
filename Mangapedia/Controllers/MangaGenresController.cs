﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mangapedia.Data;
using Mangapedia.Models;

namespace Mangapedia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MangaGenresController : ControllerBase
    {
        private readonly MangapediaContext _context;

        public MangaGenresController(MangapediaContext context)
        {
            _context = context;
        }

        // GET: api/MangaGenres
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MangaGenre>>> GetMangaGenre()
        {
          if (_context.MangaGenre == null)
          {
              return NotFound();
          }
            return await _context.MangaGenre.ToListAsync();
        }

        // GET: api/MangaGenres/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MangaGenre>> GetMangaGenre(int id)
        {
          if (_context.MangaGenre == null)
          {
              return NotFound();
          }
            var mangaGenre = await _context.MangaGenre.FindAsync(id);

            if (mangaGenre == null)
            {
                return NotFound();
            }

            return mangaGenre;
        }

        // PUT: api/MangaGenres/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMangaGenre(int id, MangaGenre mangaGenre)
        {
            if (id != mangaGenre.MangaId)
            {
                return BadRequest();
            }

            _context.Entry(mangaGenre).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MangaGenreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MangaGenres
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<MangaGenre>> PostMangaGenre(MangaGenre mangaGenre)
        {
          if (_context.MangaGenre == null)
          {
              return Problem("Entity set 'MangapediaContext.MangaGenre'  is null.");
          }
            _context.MangaGenre.Add(mangaGenre);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MangaGenreExists(mangaGenre.MangaId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetMangaGenre", new { id = mangaGenre.MangaId }, mangaGenre);
        }

        // DELETE: api/MangaGenres/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMangaGenre(int id)
        {
            if (_context.MangaGenre == null)
            {
                return NotFound();
            }
            var mangaGenre = await _context.MangaGenre.FindAsync(id);
            if (mangaGenre == null)
            {
                return NotFound();
            }

            _context.MangaGenre.Remove(mangaGenre);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool MangaGenreExists(int id)
        {
            return (_context.MangaGenre?.Any(e => e.MangaId == id)).GetValueOrDefault();
        }
    }
}
