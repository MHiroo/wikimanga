﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Linq;

namespace Mangapedia.Models
{
    public class Manga
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MangaId { get; set; }

        [Required]
        [StringLength(255)]
        public string Title { get; set; }

        public string Synopsis { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PublicationDate { get; set; }

        [StringLength(255)]
        public string ImageUrl { get; set; }

        [ForeignKey("AuthorId")]
        public int? AuthorId { get; set; }

        public ICollection<MangaGenre> MangaGenres { get; set; }
        public ICollection<ListItem> ListItems { get; set; }
        public Manga()
        {
            MangaGenres = new HashSet<MangaGenre>();
            ListItems = new HashSet<ListItem>();
        }
    }
}
