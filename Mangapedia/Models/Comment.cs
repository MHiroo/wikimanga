﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mangapedia.Models
{
    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommentId { get; set; }

        [Required]
        public string Content { get; set; }

        [Column(TypeName = "timestamp")]
        public DateTime CommentDate { get; set; }

        public int MangaId { get; set; }
        [ForeignKey("MangaId")]
        public Manga Manga { get; set; }

        [ForeignKey("UserId")]
        public int UserId { get; set; }

    }
}
