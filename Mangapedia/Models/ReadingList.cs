﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Linq;

namespace Mangapedia.Models
{
    public class ReadingList
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ListId { get; set; }

        [ForeignKey("UserId")]
        public int UserId { get; set; }
        public ICollection<ListItem> ListItems { get; set; }
        public ReadingList()
        {
            ListItems = new HashSet<ListItem>();
        }
    }
}
