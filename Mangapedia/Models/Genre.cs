﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mangapedia.Models
{
    public class Genre
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GenreId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public ICollection<MangaGenre> MangaGenres { get; set; }
        public Genre()
        {
            MangaGenres = new HashSet<MangaGenre>();
        }
    }
}
