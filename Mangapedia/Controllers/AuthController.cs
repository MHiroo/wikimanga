﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mangapedia.Data;
using Mangapedia.Models;

namespace ProjectCsharpOff.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly MangapediaContext _context;

        public AuthController(MangapediaContext context)
        {
            _context = context;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginModel login)
        {
            // Assurez-vous que le modèle LoginModel est correctement défini avec [FromBody]
            var user = await _context.User
                                            .FirstOrDefaultAsync(u => u.Email == login.Email && u.Password == login.Password);


            if (user != null)
            {
                // L'utilisateur est authentifié avec succès
                return Ok(new { Message = "Connexion réussie" });
            }

            // Informations d'identification invalides, authentification échouée
            return Unauthorized(new { Message = "Email ou mot de passe incorrect." });
        }

        // ... autres méthodes du contrôleur ...
    }
}