﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Mangapedia.Models;

namespace Mangapedia.Data
{
    public class MangapediaContext : DbContext
    {
        public MangapediaContext(DbContextOptions<MangapediaContext> options)
            : base(options)
        {
        }

        public DbSet<Mangapedia.Models.Author> Author { get; set; } = default!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Define composite key for MangaGenre
            modelBuilder.Entity<MangaGenre>()
                .HasKey(mg => new { mg.MangaId, mg.GenreId });

            // Define composite key for ListItem
            modelBuilder.Entity<ListItem>()
                .HasKey(li => new { li.ListId, li.MangaId });
        }

        public DbSet<Mangapedia.Models.Comment>? Comment { get; set; }

        public DbSet<Mangapedia.Models.Genre>? Genre { get; set; }

        public DbSet<Mangapedia.Models.ListItem>? ListItem { get; set; }

        public DbSet<Mangapedia.Models.Manga>? Manga { get; set; }

        public DbSet<Mangapedia.Models.MangaGenre>? MangaGenre { get; set; }

        public DbSet<Mangapedia.Models.ReadingList>? ReadingList { get; set; }

        public DbSet<Mangapedia.Models.User>? User { get; set; }

    }
}
