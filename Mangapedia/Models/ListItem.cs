﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mangapedia.Models
{
    public class ListItem
    {
        [ForeignKey("ReadingList")]
        public int ListId { get; set; }

        [ForeignKey("Manga")]
        public int MangaId { get; set; }
    }
}
