﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mangapedia.Data;
using Mangapedia.Models;

namespace Mangapedia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MangasController : ControllerBase
    {
        private readonly MangapediaContext _context;

        public MangasController(MangapediaContext context)
        {
            _context = context;
        }

        // GET: api/Mangas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Manga>>> GetManga()
        {
          if (_context.Manga == null)
          {
              return NotFound();
          }
            return await _context.Manga.ToListAsync();
        }

        // GET: api/Mangas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Manga>> GetManga(int id)
        {
          if (_context.Manga == null)
          {
              return NotFound();
          }
            var manga = await _context.Manga.FindAsync(id);

            if (manga == null)
            {
                return NotFound();
            }

            return manga;
        }

        // PUT: api/Mangas/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutManga(int id, Manga manga)
        {
            if (id != manga.MangaId)
            {
                return BadRequest();
            }

            _context.Entry(manga).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MangaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Mangas
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Manga>> PostManga(Manga manga)
        {
            if (_context.Manga == null)
            {
                return Problem("Entity set 'MangapediaContext.Manga' is null.");
            }

            AddImageUrlPrefix(manga); // Ajouter le préfixe à l'URL de l'image

            _context.Manga.Add(manga);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetManga", new { id = manga.MangaId }, manga);
        }


        // DELETE: api/Mangas/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteManga(int id)
        {
            if (_context.Manga == null)
            {
                return NotFound();
            }
            var manga = await _context.Manga.FindAsync(id);
            if (manga == null)
            {
                return NotFound();
            }

            _context.Manga.Remove(manga);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool MangaExists(int id)
        {
            return (_context.Manga?.Any(e => e.MangaId == id)).GetValueOrDefault();
        }
        private void AddImageUrlPrefix(Manga manga)
        {
            manga.ImageUrl = "/images/" + manga.ImageUrl;
        }

    }
}
