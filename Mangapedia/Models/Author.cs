﻿using Mangapedia.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Linq;

namespace Mangapedia.Models
{
    public class Author
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AuthorId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public string Biography { get; set; }

        public ICollection<Manga> Mangas { get; set; }
        public Author()
        {
            Mangas = new HashSet<Manga>();
        }
    }
}
