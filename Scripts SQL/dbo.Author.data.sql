﻿SET IDENTITY_INSERT [dbo].[Author] ON
INSERT INTO [dbo].[Author] ([AuthorId], [Name], [Biography]) VALUES (1, N'Akira Toriyama', N'Akira Toriyama est un mangaka japonais connu pour avoir créé des œuvres emblématiques telles que Dragon Ball et Dr. Slump.')
INSERT INTO [dbo].[Author] ([AuthorId], [Name], [Biography]) VALUES (2, N'Eiichiro Oda', N'Eiichiro Oda est un mangaka japonais célèbre pour avoir créé le manga à succès One Piece.')
INSERT INTO [dbo].[Author] ([AuthorId], [Name], [Biography]) VALUES (3, N'Masashi Kishimoto', N'Masashi Kishimoto est un mangaka japonais connu pour avoir créé le manga Naruto.')
INSERT INTO [dbo].[Author] ([AuthorId], [Name], [Biography]) VALUES (4, N'Hiromu Arakawa', N'Hiromu Arakawa est une mangaka japonaise célèbre pour son manga Fullmetal Alchemist.')
INSERT INTO [dbo].[Author] ([AuthorId], [Name], [Biography]) VALUES (5, N'Go Nagai', N'Go Nagai est un mangaka japonais réputé pour avoir créé des séries comme Mazinger Z et Devilman.')
INSERT INTO [dbo].[Author] ([AuthorId], [Name], [Biography]) VALUES (6, N'Osamu Tezuka', N'Osamu Tezuka est un mangaka japonais souvent considéré comme le père du manga moderne, il est notamment connu pour Astro Boy.')
INSERT INTO [dbo].[Author] ([AuthorId], [Name], [Biography]) VALUES (7, N'Rumiko Takahashi', N'Rumiko Takahashi est une mangaka japonaise connue pour des séries populaires comme Ranma ½ et InuYasha.')
INSERT INTO [dbo].[Author] ([AuthorId], [Name], [Biography]) VALUES (8, N'Akira Himekawa', N'Akira Himekawa est un duo de mangaka japonais célèbre pour leurs adaptations en manga des jeux vidéo The Legend of Zelda.')
INSERT INTO [dbo].[Author] ([AuthorId], [Name], [Biography]) VALUES (9, N'Makoto Yukimura', N'Makoto Yukimura est un mangaka japonais connu pour le manga Vinland Saga.')
INSERT INTO [dbo].[Author] ([AuthorId], [Name], [Biography]) VALUES (10, N'Yoshihiro Togashi', N'Yoshihiro Togashi est un mangaka japonais célèbre pour avoir créé YuYu Hakusho et Hunter x Hunter.')
INSERT INTO [dbo].[Author] ([AuthorId], [Name], [Biography]) VALUES (11, N'Tite Kubo', N'Tite Kubo est un mangaka japonais connu pour avoir créé le manga Bleach.');
INSERT INTO [dbo].[Author] ([AuthorId], [Name], [Biography]) VALUES (12, N'Hiro Mashima', N'Hiro Mashima est un mangaka japonais célèbre pour avoir créé les mangas Fairy Tail et Rave.');
SET IDENTITY_INSERT [dbo].[Author] OFF
