﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Mangapedia.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        [Required]
        [StringLength(255)]
        public string Username { get; set; }

        [Required]
        [StringLength(255)]
        public string Email { get; set; }

        [Required]
        [StringLength(255)]
        public string Password { get; set; } // Should be hashed

        [Column(TypeName = "datetime2")]
        public DateTime SignupDate { get; set; }

        public ICollection<ReadingList> ReadingLists { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public User()
        {
            ReadingLists = new HashSet<ReadingList>();
            Comments = new HashSet<Comment>();
        }
    }
}
