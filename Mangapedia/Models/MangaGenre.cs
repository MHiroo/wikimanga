﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Mangapedia.Models
{
    public class MangaGenre
    {
        [ForeignKey("Manga")]
        public int MangaId { get; set; }

        [ForeignKey("Genre")]
        public int GenreId { get; set; }
    }
}
