﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Mangapedia.Data;
var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<MangapediaContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("MangapediaContext") ?? throw new InvalidOperationException("Connection string 'MangapediaContext' not found.")));

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();
// ajout pour création bdd
#pragma warning disable CS8602 // Déréférencement d'une éventuelle référence null.
using (var serviceScope = app.Services.GetService<IServiceScopeFactory>().CreateScope())
#pragma warning restore CS8602 // Déréférencement d'une éventuelle référence null.
{
    var context = serviceScope.ServiceProvider.GetRequiredService<MangapediaContext>();
   // context.Database.EnsureDeleted();
    //context.Database.EnsureCreated();
}
app.Run();
