﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mangapedia.Data;
using Mangapedia.Models;

namespace Mangapedia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReadingListsController : ControllerBase
    {
        private readonly MangapediaContext _context;

        public ReadingListsController(MangapediaContext context)
        {
            _context = context;
        }

        // GET: api/ReadingLists
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadingList>>> GetReadingList()
        {
          if (_context.ReadingList == null)
          {
              return NotFound();
          }
            return await _context.ReadingList.ToListAsync();
        }

        // GET: api/ReadingLists/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadingList>> GetReadingList(int id)
        {
          if (_context.ReadingList == null)
          {
              return NotFound();
          }
            var readingList = await _context.ReadingList.FindAsync(id);

            if (readingList == null)
            {
                return NotFound();
            }

            return readingList;
        }

        // PUT: api/ReadingLists/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReadingList(int id, ReadingList readingList)
        {
            if (id != readingList.ListId)
            {
                return BadRequest();
            }

            _context.Entry(readingList).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReadingListExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ReadingLists
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ReadingList>> PostReadingList(ReadingList readingList)
        {
          if (_context.ReadingList == null)
          {
              return Problem("Entity set 'MangapediaContext.ReadingList'  is null.");
          }
            _context.ReadingList.Add(readingList);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetReadingList", new { id = readingList.ListId }, readingList);
        }

        // DELETE: api/ReadingLists/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteReadingList(int id)
        {
            if (_context.ReadingList == null)
            {
                return NotFound();
            }
            var readingList = await _context.ReadingList.FindAsync(id);
            if (readingList == null)
            {
                return NotFound();
            }

            _context.ReadingList.Remove(readingList);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ReadingListExists(int id)
        {
            return (_context.ReadingList?.Any(e => e.ListId == id)).GetValueOrDefault();
        }
    }
}
